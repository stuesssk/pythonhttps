import http.server
import ssl

server_address("192.168.0.1", 8080)
httpd = http.server.HTTPServer(server_address, http.server.SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket(httpd.socket, keyfile = "key.pem", certfile="cert.pem", ssl_version=ssl.PROTOCOL_TLS)